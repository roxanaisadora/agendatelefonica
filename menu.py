class MenuAgenda:

    def __init__(self):
        self.optionSwitcher = OptionSwitcher()
        print('Metodo INIT de la clase MenuAgenda')

    def mostrar(self):
        try:
            menu = [
                ['Agenda Personal'],
                ['----------------'],
                ['1. Añadir Contacto'],
                ['2. Lista de contactos'],
                ['3. Buscar contacto'],
                ['4. Editar contacto'],
                ['5. Eliminar contacto'],
                ['9. Cerrar agenda']
            ]

            print()
            print('----------------')
            for x in range(len(menu)):
                print(menu[x][0])

            print()
            option = int(input("Introduzca la opción deseada: "))
            self.validarOption(option)

        except ValueError:
            print()
            print('ERROR : Solo esta permitido el ingreso de numero....!!!')
            print()
            self.mostrar()

    # Metodo
    # Para validar que la opcion ingresada por el usuario se encuenta
    # entre en el rango permitido.
    def validarOption(self, option):
        if option == 9:
            print("Saliendo de la agenda ...")
            exit()

        if option >=1  and option <=6:
            return option

        # Mensaje de opcion incorrecta y volver a mostrar el menu
        print()
        print("Ups opcion incorrecta, intente nuevamente")
        print()
        self.mostrar()

