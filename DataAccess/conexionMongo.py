import pymongo

class ConexionMDB:

    def __init__(self):
        myclient = pymongo.MongoClient("mongodb://localhost:27017/") ##insertar host
        mydb = myclient["agendatelefonica"] #insertar
        self.cursor = mydb["contactos"] #insertar

    def agregarUno(self, objDict):
        x = self.cursor.insert_one(objDict)
        print(x.inserted_id)

    def agregarVarios(self, objList):
        x = self.cursor.insert_many(objList)
        print(x.inserted_ids)

    def traerLista(self):
        #x = self.cursor.find()
        for x in self.cursor.find():
            print(x)

    def traerData(self, myquery):
        mydoc = self.cursor.find(myquery)
        for x in mydoc:
            print(x)

    def actualizarData(self, myquery,newvalues):
        x = self.cursor.update_one(myquery, newvalues)
        print(x.modified_count, "documents updated.")

    def borrarData(self, myquery):
        x = self.cursor.delete_many(myquery)
        print(x.deleted_count, " documents deleted.")
