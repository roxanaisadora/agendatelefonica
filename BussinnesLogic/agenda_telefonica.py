#!/usr/bin/env python
# -*- coding: utf-8 -*-

class AgendaTelefonica:

    contactos = []

    def __init__(self):
        print('Metodo INIT de la clase AgendaTelefonica')

    def anadirContacto(self, **newContacto):
        self.contactos.append(newContacto)
        return True

    def listarContacto(self):
        return self.contactos

    def buscarContacto(self, documento = '99999999'):
        for contacto in self.contactos:
            if contacto['documento'] == documento:
                return contacto

    def editarContacto(self):
        pass

    def eliminarContacto(self, documento = '99999999'):
        buscar = self.buscarContacto(documento)
        if buscar is not None:
            self.contactos.remove(buscar)
            return True

